package arvato.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class BankAccountData {

    private String bankAccount;
}
