package arvato;

import configurations.ConfigurationsProperties;
import org.json.JSONArray;
import org.json.JSONObject;

public class BankAccountValidator {

    private static ConfigurationsProperties configurationsProperties;

    public BankAccountValidator() {
        configurationsProperties = new ConfigurationsProperties();
    }

    public String getIsValid(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.isValid()).toString();
    }

    public String getMessageObject(JSONObject jsonObj) {
        return jsonObj.get(configurationsProperties.message()).toString();
    }

    private JSONObject getRiskCheckMessagesObject(JSONArray jsonArray) {
        JSONObject RiskCheckMessagesObjectToJsonObject = new JSONObject();

        for (int i = 0; i < jsonArray.length(); i++) {
            RiskCheckMessagesObjectToJsonObject = jsonArray.getJSONObject(i);
        }

        return RiskCheckMessagesObjectToJsonObject;
    }

    public String getType(JSONArray jsonArray) {
        return getRiskCheckMessagesObject(jsonArray).get(configurationsProperties.type()).toString();
    }

    public String getFieldReference(JSONArray jsonArray) {
        return getRiskCheckMessagesObject(jsonArray).get(configurationsProperties.fieldReference()).toString();
    }

    public String getCode(JSONArray jsonArray) {
        return getRiskCheckMessagesObject(jsonArray).get(configurationsProperties.code()).toString();
    }

    public String getCustomerFacingMessage(JSONArray jsonArray) {
        return getRiskCheckMessagesObject(jsonArray).get(configurationsProperties.customerfacingmessage()).toString();
    }

    public String getMessage(JSONArray jsonArray) {
        return getRiskCheckMessagesObject(jsonArray).get(configurationsProperties.message()).toString();
    }

    public String getActionCode(JSONArray jsonArray) {
        return getRiskCheckMessagesObject(jsonArray).get(configurationsProperties.actionCode()).toString();
    }
}
