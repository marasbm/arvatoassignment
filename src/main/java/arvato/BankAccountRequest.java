package arvato;

import arvato.dto.BankAccountData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import configurations.ConfigurationsProperties;
import static com.jayway.restassured.RestAssured.given;



public class BankAccountRequest {

    private final ConfigurationsProperties configurationsProperties;

    private ObjectMapper objectMapper;

    private BankAccountData bankAccountData;

    public BankAccountRequest(){

        configurationsProperties = new ConfigurationsProperties();
    }

    public Response validateBankAccount(String authkey, String authvalue, String requestbody) {

        RequestSpecification httpRequest = given();

        httpRequest.header(authkey, authvalue);

        httpRequest.contentType(ContentType.JSON);

        httpRequest.body(requestbody);

        return httpRequest.post(configurationsProperties.internalEnvironment().concat(configurationsProperties.baseUrlBankAccount()));
    }

    public String bankAccountBody (String bankAccount){
       objectMapper = new ObjectMapper();

        String bAccount = null;

       try {
           bAccount = this.objectMapper.writeValueAsString(objectMapper.writeValueAsString(
                   bankAccountData.builder()
                           .bankAccount(bankAccount)
                           .build())
           );
           System.out.println("--\n" + bAccount + "\n--");
       }
       catch  (JsonProcessingException e) {
           e.printStackTrace();
       }
       return bAccount;
    }

    public String invalidBankAccountBody (){
        objectMapper = new ObjectMapper();

        String bAccount = null;

        try {
            bAccount = this.objectMapper.writeValueAsString(objectMapper.writeValueAsString(
                    bankAccountData.builder()
                            .bankAccount("GB09HAOE91311808002317")
                            .build())
            );
            System.out.println("--\n" + bAccount + "\n--");
        }
        catch  (JsonProcessingException e) {
            e.printStackTrace();
        }
        return bAccount;
    }

    public String blankSpacesBankAccountBody (){
       return "{\n" +
                "\"bankAccount\":\"                \"\n" +
                "}";
    }
}
