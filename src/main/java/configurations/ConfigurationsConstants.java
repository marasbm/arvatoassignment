package configurations;

public class ConfigurationsConstants {

    static final String ENVIRONMENT = "environment";

    public final String INTERNAL_ENVIRONMENT = "internalenvironment";

    public final String BASE_URL_BANK_ACCOUNT = "baseuribankaccount";

    final String TOKEN = "token";

    final String INVALID_TOKEN = "invalidtoken";

    final String X_AUTH_KEY = "xauthkey";

    final String IS_VALID = "isvalid";

    final String TYPE = "type";

    final String CODE = "code";

    final String MESSAGE = "message";

    final String ACTION_CODE = "actioncode";

    final String CUSTOMER_FACING_MESSAGE = "customercacingmessage";

    final String FIELD_REFERENCE = "fieldreference";
}
