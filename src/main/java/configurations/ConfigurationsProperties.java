package configurations;

public class ConfigurationsProperties {

    private final Configuration configuration;

    private final ConfigurationsConstants configurationsConstants;

    public ConfigurationsProperties(){

        configuration = new Configuration();

        configurationsConstants = new ConfigurationsConstants();
    }

    public String internalEnvironment(){
        return configuration.getConfigurationNode(configurationsConstants.INTERNAL_ENVIRONMENT);
    }

    public String baseUrlBankAccount(){
        return configuration.getConfigurationNode(configurationsConstants.BASE_URL_BANK_ACCOUNT);
    }

    public String token(){

        return configuration.getConfigurationNode(configurationsConstants.TOKEN);
    }

    public String invalidToken(){

        return configuration.getConfigurationNode(configurationsConstants.INVALID_TOKEN);
    }

    public String xAuthKey(){

        return configuration.getConfigurationNode(configurationsConstants.X_AUTH_KEY);
    }

    public String isValid(){

        return configuration.getConfigurationNode(configurationsConstants.IS_VALID);
    }

    public String type(){

        return configuration.getConfigurationNode(configurationsConstants.TYPE);
    }

    public String code(){

        return configuration.getConfigurationNode(configurationsConstants.CODE);
    }

    public String message(){

        return configuration.getConfigurationNode(configurationsConstants.MESSAGE);
    }

    public String actionCode(){

        return configuration.getConfigurationNode(configurationsConstants.ACTION_CODE);
    }

    public String fieldReference(){

        return configuration.getConfigurationNode(configurationsConstants.FIELD_REFERENCE);
    }

    public String customerfacingmessage(){

        return configuration.getConfigurationNode(configurationsConstants.CUSTOMER_FACING_MESSAGE);
    }
}
