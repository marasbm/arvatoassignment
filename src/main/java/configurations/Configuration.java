package configurations;

import org.junit.jupiter.api.Assertions;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.NoSuchElementException;
import java.util.Optional;


public class Configuration {

    private Document parsedDocument;

    private Document runConfigurationDocument;

    private Document readParameterInRunConfigurationFile(){
        try {
            if(runConfigurationDocument == null){

                Optional<String> path = Optional.of(getConfigurationFullPath());

                File file = new File(path.orElseThrow(()->new NoSuchElementException("ConfigurationsFullPath")));

                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

                runConfigurationDocument = documentBuilder.parse(file);
            }

            return runConfigurationDocument;

        } catch (Exception exception) {

            return null;
        }
    }

    private String getRunConfigurationEnvironment(){
        return Optional.ofNullable(readParameterInRunConfigurationFile())
                .map(a -> a.getElementsByTagName(ConfigurationsConstants.ENVIRONMENT))
                .map(a -> a.item(0))
                .map(Node::getTextContent).orElseThrow(()->new NoSuchElementException(ConfigurationsConstants.ENVIRONMENT));
    }

    private String getConfigurationFullPath() throws IOException {
            return getCanonicalPath() + "/configurations/qa.xml";
    }

    private String getCanonicalPath() throws IOException {
        return new File(".").getCanonicalPath();
    }

    private Document readParameterInConfigurationFile(){

        try {
            if(parsedDocument == null){

                Optional<String> path = Optional.of(getConfigurationFullPath());

                File file = new File(path.orElseThrow(()->new NoSuchElementException("ConfigurationsFullPath")));

                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

                parsedDocument = documentBuilder.parse(file);
            }
            return parsedDocument;

        } catch (Exception exception) {

            return null;
        }
    }

    public void generateExecutionReport() throws IOException{
        File file = new File(getCanonicalPath().concat("/target/site/surefire-report.html"));

        if(file.exists()){
            String dateTimeNow = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());

            String destinationFile = getCanonicalPath()
                    .concat("/reports/surefire-report").concat("_")
                    .concat(dateTimeNow).concat("_").concat("qa.html");

            File destFile = new File(destinationFile);

            if(!destFile.exists()){
                Assertions.assertTrue(file.renameTo(new File(destinationFile)));
            }
        }
    }

    String getConfigurationNode(String node) {
        return Optional.ofNullable(readParameterInConfigurationFile())
                .map(a -> a.getElementsByTagName(node))
                .map(a -> a.item(0))
                .map(Node::getTextContent).orElseThrow(()->new NoSuchElementException(node));
    }
}
