package helpers;

import com.jayway.restassured.response.Response;
import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.json.JSONArray;
import org.json.JSONObject;

public class HelpersValidator {

    public static JSONObject getResponseBodyToJsonObject(Response response) {
        return new JSONObject(response.body().prettyPrint());
    }

    public static JSONArray getResponseBodyToJsonArray(Response response){
        return new JSONArray(response.body().prettyPrint());
    }

    public static String generateValidIban() {
       Iban iban = new Iban.Builder()
                .countryCode(CountryCode.AT)
                .bankCode("19043")
                .accountNumber("00234573201")
                .build();
        return iban.toString();
    }
}
