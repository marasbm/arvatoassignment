package helpers;

import configurations.Configuration;

import java.io.IOException;

public class ExecutionReport {

    private final Configuration configuration;

    public ExecutionReport(){
        configuration = new Configuration();
    }

    public void saveReport() throws IOException {
        configuration.generateExecutionReport();
    }
}
