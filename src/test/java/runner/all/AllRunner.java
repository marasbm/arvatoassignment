package runner.all;

import arvato.InvalidBankAccountTest;
import arvato.ValidBankAccountTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;


@RunWith(JUnitPlatform.class)
@SelectClasses({InvalidBankAccountTest.class, ValidBankAccountTest.class})
public class AllRunner { }
