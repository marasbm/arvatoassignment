package runner.report;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;
import report.ReportTest;

@RunWith(JUnitPlatform.class)
@SelectClasses({ReportTest.class})
public class ReportRunner { }