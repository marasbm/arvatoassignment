package arvato;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import helpers.JiraIntegration;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ValidBankAccountTest {

    private static BankAccountRequest bankAccountRequest;

    private static ConfigurationsProperties configurationsProperties;

    private BankAccountValidator bankAccountValidator;

    private String iban;

    @BeforeAll
    public static void initClass(){

        bankAccountRequest = new BankAccountRequest();

        configurationsProperties = new ConfigurationsProperties();
    }

    @BeforeEach
    public void initTest(){

        bankAccountValidator = new BankAccountValidator();

        iban = HelpersValidator.generateValidIban();
    }

    @JiraIntegration.TestCase(id = " AAAAA-0001")
    @Test
    public void validateValidBankAccount() {

        Response response = bankAccountRequest.validateBankAccount(configurationsProperties.xAuthKey(),
                configurationsProperties.token(), bankAccountRequest.bankAccountBody(iban));

        Assertions.assertEquals(200, response.statusCode(),
                "It was expected the status code 200");

        JSONObject jObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getIsValid(jObject)),
                "It was expected IsValid field not empty");

        Assertions.assertEquals("true", bankAccountValidator.getIsValid(jObject),
                "It was expected IsValid field filled with true");
    }

    @JiraIntegration.TestCase(id = " AAAAA-0002")
    @Test
    public void validateNotValidBankAccount() throws InterruptedException  {

        Response response = bankAccountRequest.validateBankAccount(configurationsProperties.xAuthKey(),
                       configurationsProperties.token(), bankAccountRequest.invalidBankAccountBody());

        Assertions.assertEquals(200, response.statusCode(),
                "It was expected the status code 200");

        JSONObject jObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getIsValid(jObject)),
                "It was expected IsValid field not empty");

        Assertions.assertEquals("false", bankAccountValidator.getIsValid(jObject),
                "It was expected IsValid field filled with false");

        JSONArray riskCheckMessagesArray = jObject.getJSONArray("riskCheckMessages");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getType(riskCheckMessagesArray)),
                "It was expected type field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getCode(riskCheckMessagesArray)),
                "It was expected code field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getMessage(riskCheckMessagesArray)),
                "It was expected code field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getCustomerFacingMessage(riskCheckMessagesArray)),
                "It was expected customerFacingMessage field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getActionCode(riskCheckMessagesArray)),
                "It was expected actionCode field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getFieldReference(riskCheckMessagesArray)),
                "It was expected actionCode field not empty");

        Assertions.assertEquals("BusinessError", bankAccountValidator.getType(riskCheckMessagesArray),
                "It was expected 'BusinessError' in type");

        Assertions.assertEquals("200.909", bankAccountValidator.getCode(riskCheckMessagesArray),
                "It was expected '200.909' in code");

        Assertions.assertEquals("Account number from this country is not supported for this AfterPay merchant.",
                bankAccountValidator.getMessage(riskCheckMessagesArray),
                "It was expected 'Account number from this country is not supported for this AfterPay merchant.' message");

        Assertions.assertEquals("Die Kontonummer dieses Landes wird für diesen AfterPay-Händler nicht unterstützt",
                bankAccountValidator.getCustomerFacingMessage(riskCheckMessagesArray),
                "It was expected 'Die Kontonummer dieses Landes wird für diesen AfterPay-Händler nicht unterstützt' message");

        Assertions.assertEquals("AskConsumerToReEnterData", bankAccountValidator.getActionCode(riskCheckMessagesArray),
                "It was expected 'AskConsumerToReEnterData' message");

        Assertions.assertEquals("bankAccount", bankAccountValidator.getFieldReference(riskCheckMessagesArray),
                "It was expected 'bankAccount' message");
    }
}
