package arvato;

import com.jayway.restassured.response.Response;
import configurations.ConfigurationsProperties;
import helpers.HelpersValidator;
import helpers.JiraIntegration;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.api.JUnitSoftAssertions;
import org.assertj.core.api.SoftAssertions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



public class InvalidBankAccountTest {


    private static BankAccountRequest bankAccountRequest;

    private static ConfigurationsProperties configurationsProperties;

    private BankAccountValidator bankAccountValidator;


    private String iban;


    @Rule public final JUnitSoftAssertions softly = new JUnitSoftAssertions();

    @BeforeAll
    public static void initClass(){

        bankAccountRequest = new BankAccountRequest();

        configurationsProperties = new ConfigurationsProperties();


    }

    @BeforeEach
    public void initTest(){

        bankAccountValidator = new BankAccountValidator();

        iban = HelpersValidator.generateValidIban();

        SoftAssertions softly = new SoftAssertions();

    }

    @JiraIntegration.TestCase(id = " AAAAA-0003")
    @Test
    public void validateBankAccountUnauthorized() {

        Response response = bankAccountRequest.validateBankAccount(configurationsProperties.xAuthKey(),
                configurationsProperties.invalidToken(), bankAccountRequest.bankAccountBody(iban));

        softly.assertThat(response.statusCode()).as("It was expected the status code 401").isEqualTo(401);


        JSONObject jObject = HelpersValidator.getResponseBodyToJsonObject(response);

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getMessageObject(jObject)),
                "It was expected IsValid field not empty");

        Assertions.assertEquals("Authorization has been denied for this request.", bankAccountValidator.getMessageObject(jObject),
                "It was expected 'Authorization has been denied for this request.' message");
    }

    @JiraIntegration.TestCase(id = " AAAAA-0004")
    @Test
    public void validateBankAccountEmptyBankAccount() {

        SoftAssertions softly = new SoftAssertions();

        Response response = bankAccountRequest.validateBankAccount(configurationsProperties.xAuthKey(),
               configurationsProperties.token(), "");

        softly.assertThat(response.statusCode()).as("It was expected the status code 400").isEqualTo(400);

        JSONArray jArray = HelpersValidator.getResponseBodyToJsonArray(response);

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getType(jArray)),
                "It was expected type field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getCode(jArray)),
                "It was expected Code field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getMessage(jArray)),
                "It was expected Message field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getCustomerFacingMessage(jArray)),
                "It was expected CustomerFacingMessage field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getActionCode(jArray)),
                "It was expected ActionCode field not empty");

        Assertions.assertEquals("BusinessError", bankAccountValidator.getType(jArray),
                "It was expected 'BusinessError' message");

        Assertions.assertEquals("400.001", bankAccountValidator.getCode(jArray),
                "It was expected '400.001' value");

        Assertions.assertEquals("The body of the request is missing, or its format is invalid.", bankAccountValidator.getMessage(jArray),
                "It was expected 'The body of the request is missing, or its format is invalid.' message");

        Assertions.assertEquals("The body of the request is missing, or its format is invalid.", bankAccountValidator.getCustomerFacingMessage(jArray),
                "It was expected 'The body of the request is missing, or its format is invalid.' message");

        Assertions.assertEquals("Unavailable", bankAccountValidator.getActionCode(jArray),
                "It was expected 'Unavailable' message");

        softly.assertAll();

    }

    @JiraIntegration.TestCase(id = " AAAAA-0005")
    @Test
    public void validateBankAccountBlankSpacesBankAccount() {
        SoftAssertions softly = new SoftAssertions();

        Response response = bankAccountRequest.validateBankAccount(configurationsProperties.xAuthKey(),
                configurationsProperties.token(), bankAccountRequest.blankSpacesBankAccountBody());

        Assertions.assertEquals(400, response.statusCode(), "It was expected the status code 400");

        JSONArray jArray = HelpersValidator.getResponseBodyToJsonArray(response);

        softly.assertThat(bankAccountValidator.getType(jArray)).as("It was expected type field not empty").isNotEmpty();

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getType(jArray)),
                "It was expected type field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getCode(jArray)),
                "It was expected Code field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getMessage(jArray)),
                "It was expected Message field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getCustomerFacingMessage(jArray)),
                "It was expected CustomerFacingMessage field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getActionCode(jArray)),
                "It was expected ActionCode field not empty");

        Assertions.assertTrue(StringUtils.isNotEmpty(bankAccountValidator.getFieldReference(jArray)),
                "It was expected FieldReference field not empty");

        softly.assertThat(bankAccountValidator.getFieldReference(jArray)).as("It was expected FieldReference field not empty").isNotEmpty();

        softly.assertThat(bankAccountValidator.getType(jArray)).as("It was expected 'BusinessError' message").isEqualTo("BusinessError");

        softly.assertThat(bankAccountValidator.getCode(jArray)).as("It was expected '400.006' value").isEqualTo("400.006");

        softly.assertThat(bankAccountValidator.getMessage(jArray)).as("It was expected 'TA string value with minimum length 7 is required.' message")
                .isEqualTo("A string value with minimum length 7 is required.");

        softly.assertThat(bankAccountValidator.getCustomerFacingMessage(jArray)).as("Ein Wert muss eingetragen werden")
                .isEqualTo("Ein Wert muss eingetragen werden");

        softly.assertThat(bankAccountValidator.getActionCode(jArray)).as("It was expected 'AskConsumerToReEnterData' message")
                .isEqualTo("AskConsumerToReEnterData");

        softly.assertThat(bankAccountValidator.getFieldReference(jArray)).as("It was expected 'bankAccount' message")
                .isEqualTo("bankAccount");

        softly.assertAll();

    }
}
