package report;

import helpers.ExecutionReport;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class ReportTest {
    private static ExecutionReport executionReport;

    public ReportTest(){
        executionReport = new ExecutionReport();
    }

    @Test
    public void generateReport() throws IOException {
        executionReport.saveReport();
    }
}
